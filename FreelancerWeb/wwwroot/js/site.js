﻿function openPage(pageName) {
    var i;
    var x = document.getElementsByClassName("tabcontent");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(pageName).style.display = "block";
}

function showDetails(id) {
   
    $('#dettaglio'+id).show();
    $('#intestazione' +id).show();
}

function saveOrder() {
    var check = true;
    var orderName = $("#ordname").val();
    var cost = $("#cost").val();
    var billableHours = $("#billableHours").val();
    if (orderName === "")
    {
        $("#errorOrderName").show();
        check = false;
    }
    if (cost === "") {
        $("#errorcost").show();
        check = false;
    }
    if (billableHours === "") {
        $("#errorbillable").show();
        check = false;
    }
    if (check === true) {
        var Indata = { orderName: orderName, cost: cost, billableHours: billableHours };
        $.get("/home/insertOrder", Indata, function () {
            $("#ordname").val('');
            $("#cost").val('');
            $("#billableHours").val('');
            $("#errorOrderName").hide();
            $("#errorcost").hide();
            $("#errorbillable").hide();
            location.reload();
        });
    }
}

function saveTimeSheeet() {
    var check = true;
    var ordername = $("#order").val();
    var workdesc = $("#workDescription").val();
    var hour = $("#hour").val();
    if (ordername === "") {
        $("#errorOrder").show();
        check = false;
    }
    if (workdesc === "") {
        $("#errorWork").show();
        check = false;
    }
    if (hour === "") {
        $("#errorHour").show();
        check = false;
    }
    if (check===true) {      
        var Indata = { workdesc: workdesc, hour: hour, ordername: ordername };
        $.get("/home/insertTimeSheet", Indata, function () {
            $("#order").val('');
            $("#workDescription").val('');
            $("#hour").val('');
            $("#errorOrder").hide();
            $("#errorWork").hide();
            $("#errorHour").hide();
            location.reload();
        });
    }
}
function allowNumbersOnly(e) {
    var code = e.which ? e.which : e.keyCode;
    if (code > 31 && (code < 48 || code > 57)) {
        e.preventDefault();
    }
}
function openModal(id) {
    $("#myModal" + id).modal();
}

function deleteOrder(id) {
    var columnOrder = $("#columnOrder" + id).html();
    var columncost = $("#columncost" + id).html();
    var columnbillable = $("#columnbillable" + id).html();
    var Indata = { columnOrder: columnOrder, columncost: columncost, columnbillable: columnbillable };
    $.get("/home/deleteOrder", Indata, function () {
        location.reload();
    });
}

function deleteTimeSheet(idorder,id) {
    var dettOrder = $("#dettOrder" + idorder).html();
    var date = $("#detDate" + id).html();
    var hour = $("#detHour" + id).html();
    var description = $("#detDesc" + id).html();
    var Indata = { date: date, hour: hour, description: description, dettOrder: dettOrder};
    $.get("/home/deleteTimeSheet", Indata, function () {
        
        location.reload();
       
    });
}

function createInvoice(id)
{
    var ordername = $("#columnOrder" + id).html();
    var Indata = { ordername: ordername };
    $.get("/home/createInvoice",Indata,  function () {
        location.reload();
    });
}