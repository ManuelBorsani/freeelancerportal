﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using FreelancerWeb.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;


namespace FreelancerWeb.Controllers
{
    public class HomeController : Controller
    {
        string userId = string.Empty;
        public HomeController(IHttpContextAccessor httpContextAccessor)
        {

            // userId = httpContextAccessor.HttpContext.User.Identity.Name.Split('\\')[1].ToLower();
            userId = "manuelbo";
           
        }
        public IActionResult Index()
        {
            if (EntityManager.getUser(userId) == false)
            {
                return View("Error");
            }
            ViewModel _model = new ViewModel();
            string json = EntityManager.getAllOrders(userId);
            _model._orders = JsonConvert.DeserializeObject<List<Order>>(json);
            _model.userId = userId;
            return View(_model);
        }

        public void insertOrder(string orderName,double cost,int billableHours)
        {
            Order order = new Order(orderName, cost, billableHours);
            EntityManager.insertOrder(userId, order);
        }

        public void insertTimeSheet(string workdesc, int hour, string ordername)
        {
            string date = DateTime.Now.ToString();
            Detail _detail = new Detail(workdesc, hour, date);
            EntityManager.insertTimeSheet(userId, ordername, _detail);
        }

        public void deleteOrder(string columnOrder, double columncost,int columnbillable)
        {
            Order order = new Order(columnOrder, columncost, columnbillable);
            EntityManager.deleteOrder(userId, order);
        }

        public void deleteTimeSheet(string date,int hour,string description,string dettOrder)
        {
            Detail _detail = new Detail(description, hour, date);
            EntityManager.deleteTimeSheet(userId, dettOrder, _detail);
        }
        public void createInvoice(string ordername)
        {
            string json = EntityManager.getOrder(userId, ordername);
            Order _order = JsonConvert.DeserializeObject<Order>(json);
            Order order = new Order(_order.orderName, _order.cost_per_hour, _order.total_billable_hours);
            Invoice _invoice = new Invoice(_order);
            double totale =  _invoice.createInvoice();
            EntityManager.deleteOrderAndInsert(userId, _order,totale);
            
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
