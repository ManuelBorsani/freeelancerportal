﻿using iText.Html2pdf;
using iText.Kernel.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FreelancerWeb.Models
{
    public class Invoice
    {
        public Order order { get; set; }

        public Invoice(Order _order) { this.order = _order; }
        public double createInvoice()
        {

            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter hw = new XmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<body style='position: relative;width: 21cm; height: 29.7cm; margin: 0 auto; color: #555555;background: #FFFFFF; font-family: Arial, sans-serif; font-size: 14px; font-family: SourceSansPro;'><header style='padding: 90px 0;margin-bottom: 20px;border-bottom: 1px solid #AAAAAA;'><div style=' float: left;margin-top: 8px;'></div><div style='float: right;text-align: right;'><h2 style='font-size: 1.4em;font-weight: normal;margin: 0;'>Manuel Borsani</h2><div>Via Roma 86 Milano, IT</div><div>(602) 519-0450</div><div><a href='manuel.borsani@example.com'>manuel.borsani@example.com</a></div></div></div></header><div style='margin-bottom: 50px;'><div style='padding-left: 6px;border-left: 6px solid #0087C3;float: left;'><div style='color: #777777'>INVOICE TO:</div><h2 style='font-size: 1.4em;font-weight: normal;margin: 0;'>Apple</h2><div>Via Roma 40 Milano</div><div>example@icloud.com</a></div></div><div style='float:none;text-align: right;'><h1 style='color:#0087C3;font-size: 2.4em;line-height: 1em;font-weight: normal;margin: 0 0 10px 0;'>INVOICE 3-2-1 </h1><div>Date of Invoice:" + DateTime.Now + "</div></div></div><table border='0' cellspacing='0' cellpadding='0' style='width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;text-align:left'><thead><tr style='height:40px;text-align:bottom'><th></th><th style='text-align:left;'>DESCRIPTION</th><th style='text-align:left;'>PRICE</th><th style='text-align:center;'>HOUR</th><th style='text-align:right;'>TOTAL</th></tr></thead><tbody><tr style='height:50px;'><tr style='height:30px;color:white'><td>#</td>");
                    double totale = 0.0;
                    foreach (var detail in order.detail_per_day)
                    {
                        sb.Append("<tr style='height:10px;'><td style='color: black;font-size: 14px;background: #FFFFFF;width:20%;'>");
                        sb.Append(detail.date);
                        sb.Append("</td><td style='text-align: left;width:40%;'>");
                        sb.Append(detail.work_description);
                        sb.Append(" </td><td style='background: #DDDDDD;width:10%;'>");
                        sb.Append(order.cost_per_hour +"€");
                        sb.Append("</td><td style='width:10%;text-align:center'>");
                        sb.Append(detail.working_hours);
                        sb.Append("</td><td style=' background:lightgrey;color: #FFFFFF;width:20%;text-align:right;'>");
                        sb.Append(order.cost_per_hour * detail.working_hours+ "€");
                        sb.Append("</td></tr><tr style='height:30px;color:white'><td>#</td></tr></tbody><tfoot><tr style='height:50px;color:white'><td>#</td></tr><tr style='height:50px;color:white'><td>#</td></tr>");
                        totale += order.cost_per_hour * detail.working_hours;
                        sb.Append("</tr>");
                    }

                    sb.Append("<tr style='height:10px;'><td style='color: white;font-size: 14px;background: #FFFFFF;width:20%;'>#</td><td style='text-align: left;width:40%;color:white;'>#</td><td style='background: white;color:white;width:10%;'>60.5</td><td style='width:10%;text-align:center;color:white;'>8</td><td style=' background:#57B223;color:black;width:20%;text-align:right;'><b>TOTAL:  </b>");
                    sb.Append("            " + totale+ "€" + "</td></tr>");
                    string text = sb.ToString();
                    PdfWriter pdf = new PdfWriter(@"..\fileTemp\"+order.orderName+".pdf");
                     HtmlConverter.ConvertToPdf(text, pdf);
                    return totale;
                }
            }
        }
    }
}
