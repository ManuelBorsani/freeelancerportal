﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreelancerWeb.Models
{
    public class Order
    {
        public string orderName { get; set; }
        public double cost_per_hour { get; set; }
        public int total_billable_hours { get; set; }

        public List<Detail> detail_per_day { get; set; }


        public Order(string _orderName, double _cost, int _billableHours)
        {
            this.cost_per_hour = _cost;
            this.orderName = _orderName;
            this.total_billable_hours = _billableHours;
            this.detail_per_day = new List<Detail>();
        }
        public Order() { }

    }
}
