﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreelancerWeb.Models
{
    public class Detail
    {
        public int working_hours { get; set; }
        public string work_description { get; set; }
        public int hours { get; set; }
        public string date { get; set; }

        public Detail() { }
        public Detail(string _workdesc, int _working_hours, string _date)
        {
            this.work_description = _workdesc;
            this.working_hours = _working_hours;
            this.date = _date;

        }


    }
}
