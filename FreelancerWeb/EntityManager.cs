﻿using FreelancerWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FreelancerWeb
{
    public class EntityManager
    {
        public static bool getUser(string username)
        {
            string entity = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "getUser/{0}", username);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var response = client.GetAsync(url).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    entity = resultQuery.Result.ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return Convert.ToBoolean(entity);
        }

        public static string getOrder(string username,string ordername)
        {
            string entity = string.Empty;
            string host = ConfigurationManager.AppSettings["host"];
            string url = string.Empty;
           
            try
            {

                url = string.Format(host + "getOrder/{0}/{1}", username, ordername);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var response = client.GetAsync(url).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    entity = resultQuery.Result.ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return entity;
        }

        public static string getAllOrders(string username)
        {
            string entity = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "getAllOrders/{0}", username);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var response = client.GetAsync(url).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    entity = resultQuery.Result.ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return entity;
        }

        public static void insertOrder(string username, Order _order)
        {
            string result = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(_order);
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "insertOrder/{0}", username);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(url, content).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    result = resultQuery.Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
          
        }

        public static void deleteOrder(string username, Order _order)
        {
            string result = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(_order);
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "deleteOrder/{0}", username);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(url, content).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    result = resultQuery.Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public static void deleteOrderAndInsert(string username, Order _order,double totale)
        {
            string result = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(_order);
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "deleteOrderAndInsert/{0}/{1}", username,totale);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(url, content).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    result = resultQuery.Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public static void insertTimeSheet(string username,string ordername, Detail _detail)
        {
            string result = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(_detail);
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "insertTimeSheet/{0}/{1}", username,ordername);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(url, content).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    result = resultQuery.Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public static void deleteTimeSheet(string username, string ordername, Detail _detail)
        {
            string result = string.Empty;
            string host = string.Empty;
            string url = string.Empty;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(_detail);
            try
            {
                host = ConfigurationManager.AppSettings["host"];
                url = string.Format(host + "deleteTimeSheet/{0}/{1}", username, ordername);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(url, content).Result;
                    response.EnsureSuccessStatusCode();
                    var resultQuery = response.Content.ReadAsStringAsync();
                    result = resultQuery.Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

    }
}
