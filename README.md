


Freelancer Portal
================================


To start the app is necessary to set as Startup project the Freelancer to Mongo.

You'll have to select from the menu Freelancer to Mongo and not IIS Express. 
 
The database is on cloud (MongoDBAtlas) and the solution is made up of two projects: one webapi and one webapp. 

The first one is based on .netcore technology and it allows to interact with MongoDB. 

The second one is based on the same technology, but it maps objects from MongoDB using models and it controls them on HTML pages afterwards. 

The first app page shows a summary of all orders. 

The buttons on the left give the possibility to insert new orders and to add working hours to a specific order. 

Once the delevoper completes an order and so there are no more remaning hours,

an invoice can be created. You'll find it inside the folder named FileTemp. 


