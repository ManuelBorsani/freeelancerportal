﻿using Cegeka.MongoDB;

using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FreelancerToMongo
{
    public class Utils
    {
        public static MongoDBDatabase getConnectionToMongo()
        {
            string connectionString = ConfigurationManager.AppSettings["connectionString"];
            string dbName = ConfigurationManager.AppSettings["dbName"];
            return new MongoDBDatabase(connectionString, dbName);
        }

        
    }
}
