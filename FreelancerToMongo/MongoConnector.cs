﻿using Cegeka.MongoDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FreelancerToMongo
{
    public class MongoConnector
    {
        IMongoCollection<BsonDocument> coll;
        MongoDBDatabase _db;

        public bool getUser(string username)
        {
            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("users");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username) };
            List<BsonDocument> _list = coll.Find(filter).ToList();
            if (_list.Count == 0)
            {
                return false;
            }
            return true;
        }

        public string getUserDetail(string username)
        {
            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("users");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username) };
            List<BsonDocument> _list = coll.Find(filter).ToList();
            if (_list.Count == 0)
            {
                BsonDocument bdoc = _list[0].ToBsonDocument();
                return bdoc.ToJson();
            }
            return string.Empty;
        }

        public string getAllOrders(string username)
        {
            var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username) };
            List<BsonDocument> _list = coll.Find(filter).ToList();
            if (_list.Count != 0)
            {
                BsonDocument bdoc = _list[0].ToBsonDocument();
                BsonArray orders = bdoc.GetElement("orders").Value.AsBsonArray;
                return orders.ToJson(jsonWriterSettings);
            }
            return string.Empty;
        }

        public string getOrder(string username,string orderName)
        {
            var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username)};
            List<BsonDocument> _list = coll.Find(filter).ToList();
            if (_list.Count != 0)
            {
                BsonDocument bdoc = _list[0].ToBsonDocument();
                BsonArray orders = bdoc.GetElement("orders").Value.AsBsonArray;
                
                foreach (var order in orders)
                {
                    if (order.ToBsonDocument().GetElement("orderName").Value.ToString() == orderName)
                    {
                        return order.ToJson(jsonWriterSettings);
                    }
                }
                return string.Empty;
            }
            return string.Empty;
        }


        public void insertOrder(string username,string json)
        {
               
            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username) };
            BsonDocument neworder = BsonSerializer.Deserialize<BsonDocument>(json);
            BsonDocument push = new BsonDocument(new BsonElement("orders", neworder));
            BsonDocument update = new BsonDocument { new BsonElement("$push",push)};
            coll.UpdateOne(filter, update);

        }

        public void deleteOrder(string username, string json)
        {

            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username) };
            MongoDB.Bson.BsonDocument neworder = BsonSerializer.Deserialize<BsonDocument>(json);
            BsonDocument push = new BsonDocument(new BsonElement("orders", neworder));
            BsonDocument update = new BsonDocument { new BsonElement("$pull", push) };
            coll.UpdateOne(filter, update);

        }

        public void insertTimeSheet(string username,string ordername, string json)
        {

            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username),new BsonElement("orders.orderName",ordername) };
            BsonDocument Timesheet = BsonSerializer.Deserialize<BsonDocument>(json);
            int hour = (Timesheet.GetElement("working_hours").Value.ToInt32())*-1;
            Timesheet.Remove("hours");
            BsonDocument push = new BsonDocument(new BsonElement("orders.$.detail_per_day", Timesheet));
            BsonDocument update = new BsonDocument { new BsonElement("$push", push) };
            coll.UpdateOne(filter, update);
            BsonDocument inc = new BsonDocument { new BsonElement("orders.$.total_billable_hours", hour) };
            BsonDocument update2 = new BsonDocument { new BsonElement("$inc", inc) };
            coll.UpdateOne(filter, update2);

        }

        public void deleteTimeSheet(string username, string ordername, string json)
        {

            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username), new BsonElement("orders.orderName", ordername) };
            BsonDocument Timesheet = BsonSerializer.Deserialize<BsonDocument>(json);
            int hour = Timesheet.GetElement("working_hours").Value.ToInt32();
            Timesheet.Remove("hours");
            BsonDocument pull = new BsonDocument(new BsonElement("orders.$.detail_per_day", Timesheet));
            BsonDocument update = new BsonDocument { new BsonElement("$pull", pull) };
            coll.UpdateOne(filter, update);
            BsonDocument inc = new BsonDocument { new BsonElement("orders.$.total_billable_hours",hour) };
            BsonDocument update2 = new BsonDocument { new BsonElement("$inc",inc)};
            coll.UpdateOne(filter, update2);
        }

        public void deleteOrderAndInsert(string username, string json,double totale)
        {

            _db = Utils.getConnectionToMongo();
            coll = _db.GetCollection<BsonDocument>("timeSheets");
            BsonDocument filter = new BsonDocument { new BsonElement("username", username) };
            MongoDB.Bson.BsonDocument neworder = BsonSerializer.Deserialize<BsonDocument>(json);
            neworder.Remove("detail_per_day");
            BsonDocument push = new BsonDocument(new BsonElement("orders", neworder));
            BsonDocument update = new BsonDocument { new BsonElement("$pull", push) };
            coll.UpdateOne(filter, update);
            coll = _db.GetCollection<BsonDocument>("history");
            neworder.Remove("total_billable_hours");
            neworder.Add(new BsonElement("total_price", totale));
            coll.InsertOne(neworder);
        }
    }
}
