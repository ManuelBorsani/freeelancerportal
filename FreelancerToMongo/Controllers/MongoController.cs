﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FreelancerToMongo.Controllers
{
    [Route("api/[controller]")]
    public class MongoController : Controller
    {
        MongoConnector _connector;

        public MongoController()
        {
            _connector = new MongoConnector();
        }

        [HttpGet]
        [Route("getUser/{username}")]
        public bool getUser(string username)
        {
            return _connector.getUser(username);
        }



        [HttpGet]
        [Route("getAllOrders/{username}")]
        public string getAllOrders(string username)
        {
            return _connector.getAllOrders(username);
        }

        [HttpGet]
        [Route("getOrder/{username}/{ordername}")]
        public string getOrder(string username,string ordername)
        {
            return _connector.getOrder(username,ordername);
        }



        [HttpPost]
        [Route("insertOrder/{username}")]
        public void insertOrder(string username)
        {
            string json = null;
            var request = HttpContext.Request.Body;

            using (var bodyReader = new StreamReader(request))
            {
                json = bodyReader.ReadToEnd();
            }

            _connector.insertOrder(username, json);

        }

        [HttpPost]
        [Route("deleteOrder/{username}")]
        public void deleteOrder(string username)
        {
            string json = null;
            var request = HttpContext.Request.Body;

            using (var bodyReader = new StreamReader(request))
            {
                json = bodyReader.ReadToEnd();
            }

            _connector.deleteOrder(username, json);

        }

        [HttpPost]
        [Route("insertTimeSheet/{username}/{ordername}")]
        public void insertTimeSheet(string username, string ordername)
        {
            string json = null;
            var request = HttpContext.Request.Body;

            using (var bodyReader = new StreamReader(request))
            {
                json = bodyReader.ReadToEnd();
            }

            _connector.insertTimeSheet(username,ordername, json);
        }

        //deleteTimeSheet
        [HttpPost]
        [Route("deleteTimeSheet/{username}/{ordername}")]
        public void deleteTimeSheet(string username, string ordername)
        {
            string json = null;
            var request = HttpContext.Request.Body;

            using (var bodyReader = new StreamReader(request))
            {
                json = bodyReader.ReadToEnd();
            }

            _connector.deleteTimeSheet(username, ordername, json);
        }
        [HttpPost]
        [Route("deleteOrderAndInsert/{username}/{totale}")]
        public void deleteOrderAndInsert(string username,double totale)
        {
            string json = null;
            var request = HttpContext.Request.Body;

            using (var bodyReader = new StreamReader(request))
            {
                json = bodyReader.ReadToEnd();
            }

            _connector.deleteOrderAndInsert(username,json,totale);
        }
    }
}
